using Microsoft.VisualStudio.TestTools.UnitTesting;
using TaskChainingContinuation;

namespace TaskChainingAndContinuationTests
{
    [TestClass]
    public class TaskChainingAndContinuation_Tests
    {
        /// <summary>
        /// To test whether MultiplyArrayByRandomGeneratedNumber method generates an array multiplied by a random generated number.
        /// </summary>
        [TestMethod]
        public void MultiplyArrayByRandomGeneratedNumberTest()
        {
            //Arrange
            long multiplier = TaskChainingAndContinuation.GenerateTrulyRandomNumber();
            long[] initialArray = new long[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            long[] expected = new long[10];

            //Act
            for (int i = 0; i < 10; i++)
            {
                expected[i] = initialArray[i] * multiplier;
            }
            long[] actual = TaskChainingAndContinuation.MultiplyArrayByRandomGeneratedNumber(initialArray, multiplier);

            //Assert
            Assert.IsTrue(expected.SequenceEqual(actual));
        }

        /// <summary>
        /// To test whether SortArray method sorts the input array.
        /// </summary>
        [TestMethod]
        public void SortArrayTest()
        {
            //Arrange
            long[] arrayToBeSorted = new long[] { 10, 7, 9, 4, 8, 3, 5, 2, 1, 6 };
            long[] expected = new long[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

            //Act
            long[] actual = TaskChainingAndContinuation.SortArray(arrayToBeSorted);

            //Assert
            Assert.IsTrue(expected.SequenceEqual(actual));
        }

        /// <summary>
        /// To test whether AverageValue method returns the average value of the input array elements.
        /// </summary>
        [TestMethod]
        public void AverageValueTest()
        {
            //Arrange
            long[] initialArray = new long[] { 10, 7, 9, 4, 8, 3, 5, 2, 1, 6 };
            double expected = 5.5;

            //Act
            double actual = TaskChainingAndContinuation.AverageValue(initialArray);

            //Assert
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        /// To test whether CreateArray_10RandomIntegers method generates a random array of 10 integers for a given input seed value.
        /// </summary>
        [TestMethod]
        public void CreateArray_10RandomIntegersTest()
        {
            //Arrange
            int seed = 3;
            long[] expected = new long[] { 630327709, 1498044246, 1857544709, 426253993, 1203643911, 387788763, 537294307, 2034163258, 748827235, 815953056 };

            //Act
            long[] actual = TaskChainingAndContinuation.CreateArray_10RandomIntegers(seed);

            //Assert
            Assert.IsTrue(expected.SequenceEqual(actual));
        }
    }
}