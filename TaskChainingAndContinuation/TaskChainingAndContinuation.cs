﻿using System.Threading.Tasks;

namespace TaskChainingContinuation
{
    public static class TaskChainingAndContinuation
    {
        public static async Task Main()
        {
            int seed = InputValidation();

            Task<long[]> taskCreateArray_10RandomIntegers = Task.Run(() => CreateArray_10RandomIntegers(seed));

            long multiplier = GenerateTrulyRandomNumber();
            await taskCreateArray_10RandomIntegers.ContinueWith(antecedent => MultiplyArrayByRandomGeneratedNumber(antecedent.Result, multiplier), TaskContinuationOptions.RunContinuationsAsynchronously);
            await taskCreateArray_10RandomIntegers.ContinueWith(antecedent => SortArray(antecedent.Result), TaskContinuationOptions.RunContinuationsAsynchronously);
            await taskCreateArray_10RandomIntegers.ContinueWith(antecedent => AverageValue(antecedent.Result), TaskContinuationOptions.RunContinuationsAsynchronously);
        }

        public static int InputValidation()
        {
            bool flag = true;
            int seed = 0;
            while (flag)
            {
                try
                {
                    Console.Write("Enter seed value for random number generation: ");
                    string? input = Console.ReadLine();
                    if (string.IsNullOrEmpty(input))
                    {
#pragma warning disable S3928
                        throw new ArgumentNullException(nameof(input), "Seed value cannot be null or empty");
                    }

                    seed = Int32.Parse(input!);
                    if (seed >= Int32.MinValue && seed <= Int32.MaxValue)
                    {
                        flag = false;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Exception:{0}\nDetails: {1}\n", e.GetType().FullName, e.Message);
                }
            }
            return seed;
        }

        public static long[] CreateArray_10RandomIntegers(int seed)
        {
            Random rnd = new Random(seed);
            long[] array = new long[10];
            Console.WriteLine("\nGenerated Array: ");
            for (int i = 0; i < 10; i++)
            {
                array[i] = rnd.Next();
                Console.WriteLine("{0,15:N0} ", array[i]);
            }
            Console.WriteLine("\n");
            return array;
        }

        public static long[] MultiplyArrayByRandomGeneratedNumber(long[] initialArray, long multiplier)
        {
            long[] resultArray = new long[10];
            Console.WriteLine("Generated Array multiplied by {0}:", multiplier);
            for (int i = 0; i < 10; i++)
            {
                resultArray[i] = initialArray[i] * multiplier;
                Console.WriteLine("{0,25:N0} ", resultArray[i]);
            }
            Console.WriteLine("\n");
            return resultArray;
        }

        public static long[] SortArray(long[] array)
        {
            Array.Sort(array);
            Console.WriteLine("Sorted Array: ");
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("{0,15:N0} ", array[i]);
            }
            Console.WriteLine("\n");
            return array;
        }

        public static double AverageValue(long[] array)
        {
            var average = array.Average();
            Console.WriteLine("Average of array elements:{0,15:N0}", average);
            return average;
        }

        public static int GenerateTrulyRandomNumber()
        {
            Random rnd = new Random();
            return rnd.Next();
        }
    }
}
